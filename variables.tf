/*
* AWS Variables
*/
variable "region" {
  type        = string
  description = "The AWS region to deploy the resources to."
  default     = "ca-central-1"
}

variable "tag_environment" {
  type        = string
  description = "This tag is used for the environment section of the Name tag as seen in EC2 console (i.e. dev)."
  default     = "staging"
}
variable "tag_project" {
  type        = string
  description = "X-Project: A tag for identifying resources associated with a project."
  default     = "devops-ec2"
}