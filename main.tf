/////////////////////////////
///// Backend Configuration

module "tfstate-backend" {
  source  = "cloudposse/tfstate-backend/aws"
  version = "0.33.0"

  enabled     = true
  namespace   = "ec2"
  environment = var.tag_environment
  name        = "ec2-creation-devops"
  attributes  = ["state"]
  role_arn    = var.deployment_role_arn

  terraform_version                  = "0.15.1"
  terraform_backend_config_file_path = "."
  terraform_backend_config_file_name = "backend.tf"
  force_destroy                      = false

  # Use this for destroys
  # terraform_backend_config_file_name = ""
  # force_destroy                      = true
}
resource "aws_vpc" "my_vpc" {
  cidr_block = "172.16.0.0/16"

  tags = {
    Name = "tf-devops"
  }
}

resource "aws_subnet" "my_subnet" {
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "172.16.10.0/24"
  availability_zone = "ca-central-1a"

  tags = {
    Name = "tf-devops"
  }
}

resource "aws_network_interface" "foo" {
  subnet_id   = aws_subnet.my_subnet.id
  private_ips = ["172.16.10.100"]

  tags = {
    Name = "primary_network_interface"
  }
}
resource "aws_instance" "this" {
  ami                       = "ami-0dcc1e21636832c5d"
  instance_type             = "m5.large"
  host_resource_group_arn   = "arn:aws:resource-groups:us-west-2:012345678901:group/win-testhost"
  tenancy                   = "host"
}


//// Find the resource in details here https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance